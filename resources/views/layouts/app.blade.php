<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <title>Project Tracer</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link href="{{ asset('css/app.css') }}" rel="stylesheet"> 
        <link rel="stylesheet" type="text/css" href="{{asset('css/custom/form.css')}}">
        <script src="{{ url('js/app.js') }}" defer></script>    
           
    </head>
    <body>
        <div id="app">            
            <router-view :key="$route.fullPath"></router-view>            
        </div>
    </body>
</html>