export default {
    headers: {
        "Accept": "application/vnd.solo.v1+json",        
        "Content-Type": "application/json",
        "Authorization": "Bearer",
    },
    baseURL: 'http://project.tracer/api',
    responseType:"blob"
}
