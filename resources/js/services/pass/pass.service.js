import BaseService from '../base.service'

let passService = class PassService extends BaseService {

    constructor(){
        super();                        
    }

    /**
     * Call the login endpoint from the Core API
     * Endpoint: /login
     *
     * @param params
     * @returns {AxiosPromise<any>}
     */
    addPersonWithPass(params){        
        let url = this.baseURL + '/admin/individual/add';
        return super.post(url, params, {responseType:"blob"});
    }

    removePersonWithPass(id){
        let url = this.baseURL + '/admin/individual/remove/'+ id
        return super.remove(url, {responseType:"blob"})
        //necessarry ba mag gamit ug responeType:'blob' ug para asa?
        //naggamit kug remove kay mao ni ang gamit pag delete na method nga naa sa base.service
        //more convenient delete method man daw haha
    }

    uploadCSV(params){ 
        let data = new FormData();
        data.append('csv', params.csv)    
        let url = this.baseURL + '/admin/individual/csv';
        return super.post(url, data, {responseType:"blob"});
    }

    passholdersList(){ 
        let url = this.baseURL + '/admin/individual/get';
        return super.get(url);
    }    
};

export default passService
