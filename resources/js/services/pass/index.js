/**
 *
 * Services that connect directly to SOLO API
 * Only API Endpoints that are public or those that
 * do not need an authentication to call should be used here
 *
 */

import PassService from './pass.service'

let Pass = new PassService();


export {
    Pass
}
