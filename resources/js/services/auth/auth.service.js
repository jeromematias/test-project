import BaseService from '../base.service'

let authService = class AuthService extends BaseService {

    constructor(){
        super();                        
    }

    /**
     * Call the login endpoint from the Core API
     * Endpoint: /login
     *
     * @param params
     * @returns {AxiosPromise<any>}
     */
    login(params){        
        let url = this.baseURL + '/login';
        
        return super.post(url, params);
    }
    
    dummy(params = {}){        
        let url = this.baseURL + '/dummy';
        
        return super.get(url, params);
    }

    logout(){
        let url = this.baseUrl + '/logout';

        return super.post(url, {});
    }

    facebookLogin(params){
        let url = this.baseUrl + '/auth/facebook';

        return super.post(url, params);
    }
};

export default authService
