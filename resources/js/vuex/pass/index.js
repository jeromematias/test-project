import state from './pass.state';
import getters from './pass.getters';
import mutations from './pass.mutations';
import actions from './pass.actions';

export default {
    namespaced: true,
    state,
    getters,
    mutations,
    actions
}