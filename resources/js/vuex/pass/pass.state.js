export default {    
    health_statuses: [
          { id: 1, name: "Healthy" },
          { id: 2, name: "PUI" },
          { id: 3, name: "PUM" },
          { id: 4, name: "Positive" },
          { id: 5, name: "Dead" },
    ],
    pass_types: [
          { id: 1, name: "Work Pass" },
          { id: 2, name: "Home QPass" },
    ],
    schedules: [
          { id: 1, name: "MTH" },
          { id: 2, name: "TF" },
          { id: 3, name: "WS" },
          { id: 4, name: "All" },
    ],
    passholders:[],
}
