export default {
    healthStatuses(state){
        return state.health_statuses;
    },
    passTypes(state){
        return state.pass_types;
    },    
    schedules(state){
        return state.schedules;
    },    
    passHolders(state){
        return state.passholders;
    },    
    totalRows(state){
        return state.passholders.length;
    },    
}
