import Vue from 'vue';
import Vuex from 'vuex';
import VuexPersist from 'vuex-persist';

Vue.use(Vuex);
Vue.config.debug = true;

import account from './account';
import pass from './pass';

const vuexPersist = new VuexPersist({
    key: 'tracer',
    storage: localStorage
});

export default new Vuex.Store({
    modules: {        
        account, pass             
    },
    plugins: [vuexPersist.plugin]
})
