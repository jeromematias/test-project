export default {
    getUser(state) {
        return state.user;
    },
    isLoggedIn(state){
        return state.user !== null;
    }    
}
