export default {
    setUser(state, data){
        state.user = data;
    },
    logoutUser(state){
        state.user = null;
    }
}