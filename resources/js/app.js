require('./bootstrap');
window.Vue = require('vue');
window.eventHub = new Vue();
require('vue-axios-interceptors');

import store from '@/vuex';
import router from '@/routes';
import '@/plugins/bootstrap-vue'

const app = new Vue({
    el: '#app',
    router,
    store,    
    mounted()
    {        
    },
    methods:
    {       
    },
    beforeCreate()
    {
       
    }
});