import Login from '@/pages/auth/Login'
import auth from '@/middleware/auth';

import Index from '@/pages/Index'
import Dashboard from '@/pages/dashboard/Dashboard' /*path/file*/
import PassHolders from '@/pages/passholders/PassHolders'
export default [
    {
        path: '/' ,
        redirect: '/login',
        meta: {
            title: 'KUDU'            
        }
    },
    {
        path: '/login' ,
        name: 'login',
        component: Login        
    },
    {
        path:'/admin', //temporary name
        name:'admin-index', //temporary
        component:Index,
        meta:{ middleware: [auth] },
        children:[{
            path: 'dashboard',
            name: 'dashboard',
            component: Dashboard,
            meta: {
                middleware: [auth]
            }
        },
        {
            path: 'passholder',
            name: 'passholder',
            component: PassHolders,
            meta: {
                middleware: [auth]
            }
        }]
    },
]