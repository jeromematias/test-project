import store from '@/vuex';
export default function auth({ next, router }) {

    if (!store.getters['account/isLoggedIn']) {
        return router.push({ name: 'login' });
    }  

    return next();
}