<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ProjectTracer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('pass', function (Blueprint $table) {
            $table->id();
            $table->string('control_no');
            $table->string('household_no')->nullable();
            $table->bigInteger('individual_id');///owner of pass, kapareha og pass id sa individual tablle means hhold member nya
            $table->bigInteger('schedule_id'); // 1 MTh, 2 TF, 3 WS, 4 All
            $table->bigInteger('type_id');
            $table->boolean('status'); /// active or inactive/removed
            $table->timestamps();
        });

        Schema::create('schedule', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->boolean('status'); /// active or inactive/removed
            $table->timestamps();
        });

        Schema::create('type', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->text('description');
            $table->boolean('status'); /// active or inactive/removed
            $table->timestamps();
        });

        Schema::create('individual', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->text('address');
            $table->bigInteger('pass_id')->nullable(); /// household grouping
            $table->boolean('status'); /// active or inactive/removed
            $table->boolean('health_status'); /// 1 healthy, 2 PUI ,3 PUM,4 Pos, 5 Healed,6 Dead, Way kwarta
            $table->timestamps();
        });

        Schema::create('check', function (Blueprint $table) {
            $table->id();
            $table->dateTime('time_in');
            $table->dateTime('time_out');
            $table->bigInteger('individual_id'); ///
            $table->bigInteger('user_id'); /// household grouping
            $table->boolean('status'); /// active or inactive/removed
            $table->timestamps();
        });
        Schema::create('group', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->text('description');
            $table->text('address');
            $table->boolean('status'); /// active or inactive/removed
            $table->timestamps();
        });

        Schema::create('offense', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->text('description');
            $table->bigInteger('individual_id'); /// 
            $table->boolean('status'); /// active or inactive/removed
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('pass');
        Schema::dropIfExists('schedule');
        Schema::dropIfExists('type');
        Schema::dropIfExists('individual');
        Schema::dropIfExists('check');
        Schema::dropIfExists('group');
        Schema::dropIfExists('offense');

    }
}
