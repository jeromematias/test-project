<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Check extends Model
{
    //
    protected $table ="check";

    public function individual()
    {
        return $this->belongsTo('App\Models\Individual');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

}
