<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Offense extends Model
{
    //
    protected $table ="offense";

    public function individual()
    {
        return $this->belongsTo('App\Models\Individual');
    }
}
