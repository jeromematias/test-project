<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    //
    protected $table ="group";


    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
