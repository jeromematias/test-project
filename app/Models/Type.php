<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    //
    protected $table ="type";

    public function pass()
    {
        return $this->hasMany('App\Models\Type');
    }
}
