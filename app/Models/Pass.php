<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pass extends Model
{
    //
    protected $table ="pass";
    protected $fillable = [
        'control_no', 'household_no', 'individual_id', 'schedule_id','type_id','status'
    ];
    public function schedule()
    {
        return $this->belongsTo('App\Models\Schedule');
    }

    public function type()
    {
        return $this->belongsTo('App\Models\Type');
    }

    public function individual()
    {
        return $this->belongsTo('App\Models\Individual');
    }

}
