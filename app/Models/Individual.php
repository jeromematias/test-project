<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Individual extends Model
{
    //
    protected $table ="individual";
    protected $fillable = [
        'name', 'address', 'status', 'health_status','pass_id',
    ];
    public function pass()
    {
        return $this->hasMany('App\Models\Pass');
    }

    public function check()
    {
        return $this->hasMany('App\Models\Check');
    }

    public function offense()
    {
        return $this->hasMany('App\Models\Offense');
    }


}
