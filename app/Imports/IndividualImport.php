<?php

namespace App\Imports;

use App\Models\Individual;
use App\Models\Pass;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithStartRow;
// use QrCode;
use PDF;

class IndividualImport implements ToCollection
{
    public $file_name;
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function startRow(): int
    {
        return 2;
    }
    // public function model(array $row)
    // {
    //     $individual = Individual::create([
    //         "name" => $row[1],
    //         "address" => $row[6],
    //         "status" => 1,
    //         "health_status" => $row[2]
    //     ]);
    //     $pass = Pass::create([
    //         "control_no" => $row[0],
    //         "household_no" => $row[5],
    //         "individual_id" => $individual->id,
    //         "schedule_id" => $row[4],
    //         "type_id" => $row[3],
    //         "status" => 1
    //     ]);
    //     $individual = Individual::find($individual->id);
    //     $individual->pass_id = $pass->id;
    //     $individual->save();
    // }
    public function collection(Collection $rows)
    {

            // set document information
            PDF::SetTitle('PASSHOLDERS QR CODE');
            PDF::SetSubject('QR CODES');
            PDF::SetKeywords('QR CODES, PASSHOLDERS');

            // set default header data
            PDF::SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 050', PDF_HEADER_STRING);

            // set header and footer fonts
            PDF::setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
            PDF::setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

            // set default monospaced font
            PDF::SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

            // set margins
            PDF::SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
            PDF::SetHeaderMargin(PDF_MARGIN_HEADER);
            PDF::SetFooterMargin(PDF_MARGIN_FOOTER);

            // set auto page breaks
            PDF::SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

            // set image scale factor
            PDF::setImageScale(PDF_IMAGE_SCALE_RATIO);

            // set some language-dependent strings (optional)
            if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
                require_once(dirname(__FILE__).'/lang/eng.php');
                PDF::setLanguageArray($l);
            }


            // NOTE: 2D barcode algorithms must be implemented on 2dbarcode.php class file.

            // set font
            PDF::SetFont('helvetica', '', 11);

            // add a page
            PDF::AddPage('P', 'LEGAL');
            PDF::SetFont('helvetica', '', 10);
            // set style for barcode
            $style = array(
                'border' => 2,
                'vpadding' => 'auto',
                'hpadding' => 'auto',
                'fgcolor' => array(0,0,0),
                'bgcolor' => false, //array(255,255,255)
                'module_width' => 1, // width of a single module in points
                'module_height' => 1 // height of a single module in points
            );
            $isFirst = true;
            $in_row  = 0;
            $in_page  = 0;
            $ml  = 18;
            $mt  = 30;
            $caption_ml  = 41;
            $caption_mt  = 32;
              foreach ($rows as $row) 
                {
                    if ($isFirst) {
                        $isFirst = false;
                        continue;
                    }  
                    $individual = Individual::create([
                        "name" => $row[1],
                        "address" => $row[6],
                        "status" => 1,
                        "health_status" => $row[2]
                    ]);
                    $pass = Pass::create([
                        "control_no" => $row[0],
                        "household_no" => $row[5],
                        "individual_id" => $individual->id,
                        "schedule_id" => $row[4],
                        "type_id" => $row[3],
                        "status" => 1
                    ]);
                    $individual = Individual::find($individual->id);
                    $individual->pass_id = $pass->id;
                    $individual->save();
                    PDF::write2DBarcode("$pass->control_no", 'QRCODE,L', $ml, $mt, 60, 60, $style, 'N'); 
                    PDF::Text($caption_ml, $caption_mt, "$pass->control_no"."$in_page");
                    $ml+=60;
                    $caption_ml+=60;
                    $in_row++;
                    $in_page++;
                    if($in_row==3)
                    {
                        $mt+=60;
                        $ml  = 18;
                        $caption_ml  = 41;
                        $caption_mt+=60;
                        $in_row = 0;
                        if($in_page==15) 
                        {
                            $in_page=0;
                            $in_row  = 0;
                            $in_page  = 0;
                            $ml  = 18;
                            $mt  = 30;
                            $caption_ml  = 41;
                            $caption_mt  = 32;
                            PDF::AddPage('P', 'LEGAL');
                        }  
                    }

                }
            //Close and output PDF document
            $this->file_name = "qr_codes".time().".pdf";
            PDF::Output(storage_path('app/public/pdf/'.$this->file_name), 'F');


    }
}



/*CODE ENDS HERE*/  /*CODE ENDS HERE*/ /*CODE ENDS HERE*//*CODE ENDS HERE*/


/*DO NOT DELAY FOLLOWING CODES, DILI KO BRIGHT SO AKO PA NA E COPY PASTE IF MA WRONG HAHAHAH*/

        // (QrCode::format('png')
        //     ->size(300)
        //     ->generate(
        //         $pass->id.
        //         '/\/'. $pass->control_no
        //         ,'../storage/app/public/pass/qrcode.png'));


            // // set document information
            // PDF::SetTitle('TCPDF Example 050');
            // PDF::SetSubject('TCPDF Tutorial');
            // PDF::SetKeywords('TCPDF, PDF, example, test, guide');

            // // set default header data
            // PDF::SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 050', PDF_HEADER_STRING);

            // // set header and footer fonts
            // PDF::setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
            // PDF::setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

            // // set default monospaced font
            // PDF::SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

            // // set margins
            // PDF::SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
            // PDF::SetHeaderMargin(PDF_MARGIN_HEADER);
            // PDF::SetFooterMargin(PDF_MARGIN_FOOTER);

            // // set auto page breaks
            // PDF::SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

            // // set image scale factor
            // PDF::setImageScale(PDF_IMAGE_SCALE_RATIO);

            // // set some language-dependent strings (optional)
            // if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
            //     require_once(dirname(__FILE__).'/lang/eng.php');
            //     PDF::setLanguageArray($l);
            // }

            // // ---------------------------------------------------------

            // // NOTE: 2D barcode algorithms must be implemented on 2dbarcode.php class file.

            // // set font
            // PDF::SetFont('helvetica', '', 11);

            // // add a page
            // PDF::AddPage('P', 'LEGAL');
            // PDF::SetFont('helvetica', '', 10);
            // // set style for barcode
            // $style = array(
            //     'border' => 2,
            //     'vpadding' => 'auto',
            //     'hpadding' => 'auto',
            //     'fgcolor' => array(0,0,0),
            //     'bgcolor' => false, //array(255,255,255)
            //     'module_width' => 1, // width of a single module in points
            //     'module_height' => 1 // height of a single module in points
            // );

            // // first row - add 45 to ML
            // // ML,MT, WIDTH, height
            // PDF::write2DBarcode($pass->control_no, 'QRCODE,L', 18, 10, 45, 45, $style, 'N');
            // PDF::Text(32, 10, $pass->control_no);
            // PDF::write2DBarcode($pass->control_no, 'QRCODE,L', 63, 10, 45, 45, $style, 'N');
            // PDF::Text(77, 10, $pass->control_no);
            // PDF::write2DBarcode($pass->control_no, 'QRCODE,L', 108, 10, 45, 45, $style, 'N');
            // PDF::Text(122, 10, $pass->control_no);
            // PDF::write2DBarcode($pass->control_no, 'QRCODE,L', 153, 10, 45, 45, $style, 'N');
            // PDF::Text(167, 10, $pass->control_no);
            // // 2nd row- add 45 to mt, 
            // PDF::write2DBarcode($pass->control_no, 'QRCODE,L', 18, 55, 45, 45, $style, 'N');
            // PDF::Text(32, 55, $pass->control_no);
            // PDF::write2DBarcode($pass->control_no, 'QRCODE,L', 63, 55, 45, 45, $style, 'N');
            // PDF::Text(77, 55, $pass->control_no);
            // PDF::write2DBarcode($pass->control_no, 'QRCODE,L', 108, 55, 45, 45, $style, 'N');
            // PDF::Text(122, 55, $pass->control_no);
            // PDF::write2DBarcode($pass->control_no, 'QRCODE,L', 153, 55, 45, 45, $style, 'N');
            // PDF::Text(167, 55, $pass->control_no);


            // //Close and output PDF document
            // PDF::Output(storage_path('app/public/pdf/hello'.time().'.pdf'), 'F');