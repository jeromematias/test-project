<?php

namespace App\Http\Controllers\Passport;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Services\Auth\AuthServiceContract;

class AuthController extends Controller
{
    protected $auth;
    
    public function __construct(AuthServiceContract $service)
    {
        $this->auth = $service;
    }

    public function passportLogin(Request $request)
    {                
        return $this->auth->login($request);
    }

    public function passportRegister(Request $request)
    {
        return $this->auth->register($request);
    }
}
