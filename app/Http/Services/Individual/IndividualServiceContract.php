<?php

namespace App\Http\Services\Individual;

use Illuminate\Http\Request;
use App\Http\Repositories\IndividualRepository;

interface IndividualServiceContract {    
	public function addIndividual(Request $request);
	public function addPassID($info);
	public function addIndividualCSV(Array $array);
	//gi access ni controller
	public function removeIndividual($id);


}