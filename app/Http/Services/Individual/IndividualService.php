<?php

namespace App\Http\Services\Individual;

use Illuminate\Http\Request;
use App\Http\Repositories\Interfaces\IndividualRepositoryContract;

class IndividualService implements IndividualServiceContract{

    protected $individualRepo;

    public function __construct(IndividualRepositoryContract $individualRepo)
    {        
        $this->individualRepo = $individualRepo;
    }

    public function addIndividual(Request $request){
    	// validation

    	return $this->individualRepo->addIndividual($request); /// dapat naay receiver dere sa pag create sa pass og sa QR
    }

    public function addPassID($info){
    	// validation
    	return $this->individualRepo->addPassID($info); 

    }

    public function addIndividualCSV(Array $array){
        // validation
        return $this->individualRepo->addIndividualCSV($array);

    }

      public function removeIndividual($id){
        // validation
        return $this->individualRepo->removeIndividual($id); 
    }
}