<?php

namespace App\Http\Services\Pass;

use Illuminate\Http\Request;
use App\Http\Repositories\PassRepository;

interface PassServiceContract {    
	public function createPass(Request $request);
	public function get();
}