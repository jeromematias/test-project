<?php

namespace App\Http\Services\Pass;

use Illuminate\Http\Request;
use App\Http\Repositories\Interfaces\PassRepositoryContract;

class PassService implements PassServiceContract{

    protected $passRepo;

    public function __construct(PassRepositoryContract $passRepo)
    {        
        $this->passRepo = $passRepo;
    }

    public function createPass(Request $request){
    	// validation

    	return $this->passRepo->createPass($request); /// dapat naay receiver dere sa pag create sa pass og sa QR

    }

    public function get(){
    	return $this->passRepo->get();
    }
}