<?php

namespace App\Http\Services\Auth;

use Illuminate\Http\Request;
use App\Http\Repositories\Interfaces\UserRepositoryContract;
use Carbon\Carbon;

class AuthService implements AuthServiceContract{

    protected $userRepo;

    public function __construct(UserRepositoryContract $userRepo)
    {        
        $this->userRepo = $userRepo;
    }

    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string'            
        ]);

        return $this->userRepo->userLogin($request);
    }

    public function register(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6',
            'mobile' => 'required',
        ]);
        
        return $this->userRepo->registerUser($request);
    }
}