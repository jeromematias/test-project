<?php

namespace App\Http\Services\Auth;

use Illuminate\Http\Request;
use App\Http\Repositories\UserRepository;
use App\User;

interface AuthServiceContract {    

    public function login(Request $request);

    public function register(Request $request);

}