<?php

namespace App\Http\Repositories;

use Illuminate\Http\Request;
use App\Http\Repositories\Interfaces\PassRepositoryContract;
use App\Models\Pass;
// use QrCode;

class PassRepository implements PassRepositoryContract{

    protected $model;

    public function __construct(Pass $pass)
    {
        $this->model = $pass;
    }

    public function createPass(Request $request)
    {
    	$pass = $this->model->create([
    		"control_no" => $request->control_no,
    		"household_no" => $request->household_no,
            "individual_id" => $request->individual_id,
            "schedule_id" => $request->schedule["id"],
            "type_id" => $request->passtype["id"],
            "status" => 1
    	]);

        // (QrCode::format('png')
        //     ->size(300)
        //     ->color(0,0,255)
        //     ->generate(
        //         $pass->id.
        //         ':::'. $pass->control_no
        //         ,'../storage/app/public/pass/qrcode'.time().'.png'));
    	return $pass;
    }

    public function get(){
        return Pass::with("individual")->where('status', 1)->get();
    }

}