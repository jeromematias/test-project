<?php

namespace App\Http\Repositories;

use Illuminate\Http\Request;
use App\Http\Repositories\Interfaces\IndividualRepositoryContract;
use App\Models\Individual;

class IndividualRepository implements IndividualRepositoryContract{

    protected $model;

    public function __construct(Individual $individual)
    {
        $this->model = $individual;
    }

    public function addIndividual(Request $request){
    	$individual = $this->model->create([
    		"name" => $request->fullname,
    		"address" => $request->address,
            "pass_id" => $request->pass_id,
            "status" => 1,
            "health_status" => $request->health_status
    	]);

        return $individual->id;
    }

    public function addIndividualCSV(Array $array){
        $individual = $this->model->create([
            "name" => $array->fullname,
            "address" => $array->address,
            "status" => 1,
            "health_status" => $array->health_status
        ]);
        return $individual->id;
    }
 
    public function addPassID($info)
    {
        $individual = Individual::find($info["individual_id"]);
        $individual->pass_id = $info["pass_id"];
        $individual->save();
        return response()->json([
            'message' => "Successful"
        ]);
    }

    public function removeIndividual($id){
        //$this->model == Individual
        return $this->model::where('id', $id)->get();
        //ang gi access sa controller
    }



}