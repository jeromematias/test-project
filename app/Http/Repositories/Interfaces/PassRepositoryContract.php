<?php

namespace App\Http\Repositories\Interfaces;

use Illuminate\Http\Request;

interface PassRepositoryContract {
	public function createPass(Request $request);
	public function get();
}