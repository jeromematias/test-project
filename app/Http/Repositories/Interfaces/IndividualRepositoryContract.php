<?php

namespace App\Http\Repositories\Interfaces;

use Illuminate\Http\Request;

interface IndividualRepositoryContract {
	public function addIndividual(Request $request);
	public function addPassID($info);
	public function addIndividualCSV(Array $array);
	public function removeIndividual($id);
}