<?php

namespace App\Http\Repositories\Interfaces;

use Illuminate\Http\Request;

interface UserRepositoryContract {
    
    public function registerUser(Request $request);

    public function userLogin(Request $request);

}