<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Http\Services\Auth\AuthServiceContract;
use App\Http\Services\Individual\IndividualServiceContract;
use App\Http\Services\Pass\PassServiceContract;
use App\Http\Repositories\Interfaces\UserRepositoryContract;
use App\Http\Repositories\Interfaces\IndividualRepositoryContract;
use App\Http\Repositories\Interfaces\PassRepositoryContract;
use App\Http\Services\Auth\AuthService;
use App\Http\Services\Individual\IndividualService;
use App\Http\Services\Pass\PassService;
use App\Http\Repositories\UserRepository;
use App\Http\Repositories\IndividualRepository;
use App\Http\Repositories\PassRepository;
use App\User;
use App\Models\Individual;
use App\Models\Pass;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
 /*registration sa repo*/
        $this->app->singleton(UserRepositoryContract::class, function ($app) {
            return new UserRepository(new User);
        });
        $this->app->singleton(IndividualRepositoryContract::class, function ($app) {
            return new IndividualRepository(new Individual);
        });

        $this->app->singleton(PassRepositoryContract::class, function ($app) {
            return new PassRepository(new Pass);
        });

/*registration sa service*/        
        $this->app->singleton(AuthServiceContract::class, function ($app) {
            return new AuthService(new UserRepository(new User));
        });        

        $this->app->singleton(IndividualServiceContract::class, function ($app) {
            return new IndividualService(new IndividualRepository(new Individual));
        });        

        $this->app->singleton(PassServiceContract::class, function ($app) {
            return new PassService(new PassRepository(new Pass));
        });        
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
